<?php
require_once('RssData.php');

class PodcastData extends RssData
{
    /**
     * Construct an instance of a podcast data finder. The $xPath parameter
     * should contain a DOMDocument with the podcast data in "rss/channel".
     *
     * @param DOMXPath &$xPath A DOMXPath instance
     */
    public function __construct(&$xPath)
    {
        parent::__construct($xPath, $xPath->query('/rss/channel')->item(0));
    }

    /**
     * Return the podcast language code. 'sv' if Swedish, 'en' if English etc.
     *
     * @return string The language code of the podcast.
     */
    public function getLanguage()
    {
        // Find the language node
        $value = $this->getNodeValue('language', 'dc:language');

        // If a language could be found, only take the first part of the code.
        // For an example "sv-SE" will be only "sv"
        if (!empty($value))
        {
            $parts = explode('-', $value);
            $value = $parts[0];
        }

        return $value;
    }

    /**
     * Return if the podcast is tagged as explicit.
     *
     * @return string Returns 'yes' or 'no', and 'no' if the tag can't be found.
     */
    public function getExplicit()
    {
        $value = $this->getNodeValue('itunes:explicit');

        if (empty($value))
        {
            return "no";
        }

        return $value;
    }

    /**
     * Return the podcast image.
     *
     * @return string Image URL string
     */
    public function getImage()
    {
        // Find the itunes:image node
        $imageNode = $this->getNode('itunes:image');

        // If there is a itunes:image node, return the href attribute
        if (!empty($imageNode))
        {
            $hrefAttribute = $imageNode->attributes->getNamedItem('href');
            if ($hrefAttribute != null)
                return $hrefAttribute->nodeValue;
        }
        // If there isn't a itunes:image node
        // Return the value of the <image><link></link></image>. The string
        // is empty if these tags can't be found
        return $this->getNodeValue('image/url');
    }

    /**
     * Return when the feed was last updated.
     *
     * @return string Date on the form (Y-m-d H:i:s)
     */
    public function getLastUpdated()
    {
        $value = $this->getNodeValue('pubDate');
        
        if (empty($value))
        {
            $value = $this->getNodeValue('lastBuildDate');
        }

        if (!empty($value))
        {
            $value = gmdate("Y-m-d H:i:s", strtotime($value));
        }

        return $value;
    }

    /**
     * Return a list of the itunes categories 
     *
     * @return string String of itunes categories
     */
    public function getCategories()
    {
        //$categoryNodes = $doc->getElementsByTagNameNS($itunesNs, 'category');
        $categoryNodes = $this->_xPath->
                         query('/rss/channel/itunes:category');

        if ($categoryNodes == null) {
            return "";
        }

        $categories = "";
        foreach ($categoryNodes as $node)
        {
            $categories .= $node->attributes->getNamedItem('text')->nodeValue
                . ", ";
        }

        $categories = substr($categories, 0, -2);
        return $categories;
    }
}
