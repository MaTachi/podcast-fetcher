<?php
require_once('../lib/simpletest/autorun.php');

class AllTests extends TestSuite
{
    function __construct()
    {
        parent::__construct();
        $this->collect(dirname(__FILE__) . '/Test',
                       new SimplePatternCollector('/Test.php/'));
    }
}
