<?php
class RssData
{
    // Reference to a DOMXPath
    protected $_xPath;

    // Reference to the node that the XPath expressions should be relative from
    protected $_contextNode;

    /**
     * Construct an instance of a episode data finder.
     *
     * @param  DOMXPath &$xPath      A DOMXPath instance
     * @param  DOMNode  $contextNode Reference to the context node
     */
    public function __construct(&$xPath, $contextNode)
    {
        echo is_null($contextNode);
        // Register namespaces
        $xPath->registerNamespace('feedburner',
                               $contextNode->lookupNamespaceURI('feedburner'));
        $xPath->registerNamespace('enclosure',
                               $contextNode->lookupNamespaceUri('enclosure'));
        $xPath->registerNamespace('media',
                               $contextNode->lookupNamespaceUri('media'));
        $xPath->registerNamespace('itunes',
                               $contextNode->lookupNamespaceUri('itunes'));
        $xPath->registerNamespace('dc',
                               $contextNode->lookupNamespaceUri('dc'));

        $this->_xPath        = $xPath;
        $this->_contextNode  = $contextNode;
    }

    /**
     * Get the value of a node. This method supports multiple string arguments.
     *
     * @param  string XPath expressions, ordered by priority
     * @return string Value of the tag name/node
     */
    public function getNodeValue()
    {
        $value;

        // Walk through all expressions given as parameters
        foreach (func_get_args() as $expression)
        {
            $value = $this->getSingleNodeValue($expression);
            // Break the loop if the node could be found, otherwise try the
            // next node
            if (!empty($value))
                break;
        }
        return $value;
    }

    /**
     * Get the value of a node.
     *
     * @param  string $expression XPath expression
     * @return string             Value of the tag name/node
     */
    private function getSingleNodeValue($expression)
    {
        $node = $this->getNode($expression);
        if (is_null($node))
            return "";
        else
            return trim($node->nodeValue);
    }

    /**
     * Get a node.
     *
     * @param  string  $expression XPath expression
     * @return DOMNode             Reference to a DOMNode
     */
    public function getNode($expression)
    {
        $entries = $this->_xPath->query($expression, $this->_contextNode);

        if ($entries->length == 0)
            return null;
        else
            return $entries->item(0);
    }
}
