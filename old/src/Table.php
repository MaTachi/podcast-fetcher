<?php
require_once('Database.php');

class Table
{
    protected $_database;

    protected $_tableName;

    public function __construct($database, $tableName) {
        $this->_database = $database;
        $this->_tableName = $tableName;
    }

    public function getRows()
    {
        $result = $this->_database->query("SELECT * FROM '$_tableName'");
        return $result;
    }

    public function getLength()
    {
        return mysql_num_rows($this->getRows());
    }

    protected function getProperty($where, $property)
    {
        $sql = "SELECT {$property} "
             . "FROM $this->_tableName "
             . "WHERE {$where}";
        $row = mysql_fetch_row($this->_database->query($sql));
        return $row[0];
    }
}
