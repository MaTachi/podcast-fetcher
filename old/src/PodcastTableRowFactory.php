<?php
require_once('TableRow.php');
require_once('PodcastData.php');

class PodcastTableRowFactory
{
    public function getInsertTableRow(&$xPath, $tableName, $rssFeed)
    {
        // Data about the podcast
        $podcastData = new PodcastData($xPath);

        // Create a new table row
        $tableRow = new TableRow($tableName);

        // Add data columns to the table row
        $tableRow->add(
            'rss', $rssFeed);
        $tableRow->add(
            'title', $podcastData->getNodeValue('title'));
        $tableRow->add(
            'link', $podcastData->getNodeValue('link'));
        $tableRow->add(
            'description', $podcastData->getNodeValue('description'));
        $tableRow->add(
            'language', $podcastData->getLanguage());
        $tableRow->add(
            'subtitle', $podcastData->getNodeValue('itunes:subtitle',
                                                   'description',
                                                   'itunes:summary'));
        $tableRow->add(
            'summary', $podcastData->getNodeValue('itunes:summary',
                                                  'description',
                                                  'itunes:subtitle'));
        $tableRow->add(
            'keywords', $podcastData->getNodeValue('itunes:keywords'));
        $tableRow->add(
            'explicit', $podcastData->getExplicit());
        $tableRow->add(
            'ownerName', $podcastData->getNodeValue('itunes:owner/itunes:name',
                                                    'itunes:author',
                                                    'title'));
        $tableRow->add(
            'ownerEmail', $podcastData->getNodeValue(
                              'itunes:owner/itunes:email'));
        $tableRow->add(
            'image', $podcastData->getImage());
        $tableRow->add(
            'author', $podcastData->getNodeValue('itunes:author',
                                                 'title'));
        $tableRow->add(
            'lastUpdated', $podcastData->getLastUpdated());
        $tableRow->add(
            'categories', $podcastData->getCategories());
        $tableRow->add(
            'added', gmdate('Y-m-d H:i:s'));

        return $tableRow;
    }

    public function getUpdateTableRow(&$xPath, $tableName, $rssFeed)
    {
        // Data about the podcast
        $podcastData = new PodcastData($xPath);

        // Create a new table row
        $tableRow = new TableRow($tableName);

        // Add data columns to the table row
        $tableRow->add(
            'rss', $rssFeed);
        $tableRow->add(
            'title', $podcastData->getNodeValue('title'));
        $tableRow->add(
            'link', $podcastData->getNodeValue('link'));
        $tableRow->add(
            'description', $podcastData->getNodeValue('description'));
        $tableRow->add(
            'language', $podcastData->getLanguage());
        $tableRow->add(
            'subtitle', $podcastData->getNodeValue('itunes:subtitle',
                                                   'description',
                                                   'itunes:summary'));
        $tableRow->add(
            'summary', $podcastData->getNodeValue('itunes:summary',
                                                  'description',
                                                  'itunes:subtitle'));
        $tableRow->add(
            'keywords', $podcastData->getNodeValue('itunes:keywords'));
        $tableRow->add(
            'explicit', $podcastData->getExplicit());
        $tableRow->add(
            'ownerName', $podcastData->getNodeValue('itunes:owner/itunes:name',
                                                    'itunes:author',
                                                    'title'));
        $tableRow->add(
            'ownerEmail', $podcastData->getNodeValue(
                              'itunes:owner/itunes:email'));
        $tableRow->add(
            'image', $podcastData->getImage());
        $tableRow->add(
            'author', $podcastData->getNodeValue('itunes:author',
                                                 'title'));
        $tableRow->add(
            'lastUpdated', $podcastData->getLastUpdated());
        $tableRow->add(
            'categories', $podcastData->getCategories());

        return $tableRow;
    }
}
