<?php
require_once('Database.php');

class MySqlDatabase implements Database
{
    // MySQL link identifier
    private $_link;

    public function __construct() {
        $this->_link = $this->getDatabaseLink();
    }

    /**
     * Connect to the database and return a link identifier.
     *
     * @return A MySQL link identifier
     */
    private function getDatabaseLink()
    {
        // Connect to the server
        $link = mysql_connect('localhost', 'root', '');

        // Check if the connection attempt was successful
        if (!$link)
        {
            die('Could not connect: ' . mysql_error());
        }

        // Set the database to `podcast`
        mysql_select_db('podcast', $link);

        // Set the charset to utf8. This is needed to make special characters
        // work correctly.
        mysql_set_charset('utf8', $link);

        // Return the MySQL link identifier
        return $link;
    }

    /**
     * Query the database with the given SQL code.
     *
     * @param  string  $sql SQL
     * @return              A MySQL resource
     */
    public function query($sql)
    {
        // Make a MySQL query
        $result = mysql_query($sql, $this->_link);

        // Check if the query was successful
        if (!$result)
        {
            die('Could not query: ' . mysql_error());
        }

        // Return the result resource from the query
        return $result;
    } 
}
