<?php
interface Database
{

    /**
     * Query the database with the given SQL code.
     *
     * @param  string  $sql SQL
     * @return              A MySQL resource
     */
    public function query($sql);
}
