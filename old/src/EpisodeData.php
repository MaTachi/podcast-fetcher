<?php
require_once('RssData.php');

class EpisodeData extends RssData
{
    /**
     * Construct an instance of a episode data finder.
     *
     * @param  DOMXPath &$xPath      A DOMXPath instance
     * @param  DOMNode  $contextNode Reference to the context node
     */
    public function __construct(&$xPath, $contextNode)
    {
        parent::__construct($xPath, $contextNode);
    }

    /**
     * Return when the episode was published.
     *
     * @return string Date on the form (Y-m-d H:i:s)
     */
    public function getPublishDate()
    {
        $value = $this->getNodeValue('pubDate');

        if (empty($value))
        {
            $value = $this->getNodeValue('dc:date');
        }

        if (!empty($value))
        {
            $value = gmdate("Y-m-d H:i:s", strtotime($value));
        }

        return $value;
    }

    /**
     * Get the duration of the episode.
     *
     * @return string Time on the form (H:i:s)
     */
    public function getDuration()
    {
        $duration = $this->getNodeValue('itunes:duration');
        $duration = $this->fixTimeFormat($duration);
        return $duration;
    }

    /**
     * Format a time to H:i:s with correct number of hours, minutes etc.
     *
     * @param  string Time on the form (s), (m:s) or (H:i:s)
     * @return string Time on the form (H:i:s)
     */
    private function fixTimeFormat($time)
    {
        if (!empty($time))
        {
            // Fix so there are hours, minutes and seconds parts in the time
            if (strlen($time) < 8)
            {
                // If the time has the format (*)
                if (preg_match('/^\d*$/', $time))
                {
                    $time = '00:00:' . $time;
                }
                // If the time has the format (*:*)
                else if (preg_match('/^\d*:\d*$/', $time))
                {
                    $time = '00:' . $time;
                }
            }

            // Fix so there is a correct number of hours, minutes and seconds. 
            // 00:90:00 should be converted to 01:30:00.
            $timeParts = explode(':', $time);
            $seconds = $timeParts[0] * 3600 + $timeParts[1] * 60
                                            + $timeParts[2];
            $time = gmdate('H:i:s', $seconds);
        }
        return $time;
    }

    /**
     * Get the URL to the media file.
     *
     * @return string URL to the media file
     */
    public function getMediaUrl()
    {
        $mediaNode = $this->getNode('enclosure');
        if (is_null($mediaNode))
        {
            $mediaNode = $this->getNode('media:content');
        }
        if (!is_null($mediaNode))
        {
            $url = $mediaNode->attributes->getNamedItem('url');
            if (!is_null($url))
            {
                return $url->nodeValue;
            }
        }
        return "";
    }

    /**
     * Get the file size of the media file in bytes.
     *
     * @return string The file size in bytes.
     */
    public function getMediaFileSize()
    {
        $mediaNode = $this->getNode('enclosure');
        if (is_null($mediaNode))
        {
            $mediaNode = $this->getNode('media:content');
        }
        if (!is_null($mediaNode))
        {
            $url = $mediaNode->attributes->getNamedItem('length');
            if (is_null($url))
            {
                $url = $mediaNode->attributes->getNamedItem('fileSize');
            }
            if (!is_null($url))
            {
                return $url->nodeValue;
            }
        }
        return "";
    }

    /**
     * Get the file type of the media file. Probably "mp3", "ogg" or "m4a".
     *
     * @return string The file type.
     */
    public function getMediaType()
    {
        $mediaNode = $this->getNode('enclosure');
        if (is_null($mediaNode))
        {
            $mediaNode = $this->getNode('media:content');
        }
        if (!is_null($mediaNode))
        {
            $url = $mediaNode->attributes->getNamedItem('type');
            if (!is_null($url))
            {
                $urlValue = $url->nodeValue;
                switch ($urlValue)
                {
                case "audio/mpeg" :
                    return "mp3";
                case "audio/ogg" :
                    return "ogg";
                case "audio/x-m4a" :
                    return "m4a";
                default: 
                    return $urlValue;
                }
            }
        }
        return "";
    }

    /**
     * Return if the podcast is tagged as explicit.
     *
     * @return string Returns 'yes' or 'no', and 'no' if the tag can't be found
     */
    public function getExplicit()
    {
        $value = $this->getNodeValue('itunes:explicit');

        if (empty($value))
        {
            return "no";
        }

        return $value;
    }
}
