<?php
require('Podcasts.php');
require('MySqlDatabase.php');

function getPodcasts()
{
    $podcasts = new Podcasts(new MySqlDatabase());
    return $podcasts->getPodcastList();
}

function getPodcast($id)
{
    $podcasts = new Podcasts(new MySqlDatabase());
    return $podcasts->getPodcast($id);
}

function addFromRssFeed($rssFeed)
{
    $podcasts = new Podcasts(new MySqlDatabase());
    $podcasts->addFromRssFeed($rssFeed);
    return true;
}

$possible_url = array('getPodcasts', 'getPodcast', 'addPodcast');

$value = "An error has occurred";

if (isset($_GET["action"]) && in_array($_GET["action"], $possible_url))
{
    switch ($_GET["action"])
    {
        case 'getPodcasts':
            $value = getPodcasts();
            break;
        case 'getPodcast':
            if (isset($_GET["id"]))
                $value = getPodcast($_GET['id']);
            else
                $value = 'Missing argument';
            break;
        case 'addPodcast':
            if (isset($_GET["rss"]))
                $value = addFromRssFeed($_GET['rss']);
            else
                $value = 'Missing argument';
            break;
    }
}

exit(json_encode($value));
