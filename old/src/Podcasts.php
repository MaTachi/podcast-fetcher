<?php
require_once('Database.php');
require_once('Table.php');
require_once('TableRow.php');
require_once('PodcastTableRowFactory.php');

class Podcasts extends Table
{

    public function __construct(Database $database) {
        parent::__construct($database, 'podcasts');
    }

    /**
     * Return the title from the row with the given ID.
     *
     * @param  string $id
     * @return string
     */
    public function getTitle($id)
    {
        return $this->getProperty("`id`={$id}", "title");
    }

    /**
     * Return the ID from the from with the given RSS URL.
     *
     * @param  string $rssFeed
     * @return string
     */
    public function getId($rssFeed)
    {
        return $this->getProperty("`rss`='$rssFeed'", "id");
    }

    /**
     * Return the RSS URL from the row with the given ID.
     *
     * @param  string $id
     * @return string
     */
    public function getRss($id)
    {
        return $this->getProperty("`id`={$id}", "rss");
    }

    /**
     * Return the description from the row with the given ID.
     *
     * @param  string $id
     * @return string
     */
    public function getDescription($id)
    {
        return $this->getProperty("`id`={$id}", "description");
    }

    public function getPodcast($id)
    {
        $sql = "SELECT * FROM `podcasts` WHERE `id`={$id}";
        $row = mysql_fetch_array($this->_database->query($sql));
        $data = array(
            'title' => $row['title'],
            'rss'   => $row['rss']
        );
        return $data;
    }

    public function getPodcastList()
    {
        $sql = "SELECT * FROM `podcasts`";
        $result = $this->_database->query($sql);
        $list = array();
        while ($row = mysql_fetch_array($result))
        {
            array_push(
                $list,
                array(
                    'title' => $row['title'],
                    'rss'   => $row['rss']
                )
            );
        }
        return $list;
    }

    public function addFromRssFeed($rssFeed)
    {
        // Prepare the DOMDocument
        $doc = new DOMDocument();
        $doc->load($rssFeed);
        $xPath = new DOMXpath($doc);

        $tableRowFactory = new PodcastTableRowFactory();
        $tableRow = $tableRowFactory->getInsertTableRow(
            $xPath, $this->_tableName, $rssFeed
        );

        // Run the mysql query
        $this->_database->query($tableRow->getInsertSql());
    }

    public function updateFromRssFeed($rssFeed)
    {
        $doc = new DOMDocument();
        $doc->load($rssFeed);
        $xPath = new DOMXpath($doc);

        $tableRowFactory = new PodcastTableRowFactory();
        $tableRow = $tableRowFactory->getUpdateTableRow(
            $xPath, $this->_tableName, $rssFeed
        );

        $this->_database->query(
            $tableRow->getUpdateSql("rss='" . $rssFeed . "'")
        );
    }

    //public function updateId($rssFeed)
    //{
    //    $doc = new DOMDocument();
    //    $doc->load($rssFeed);
    //    $xPath = new DOMXpath($doc);

    //    $tableRowFactory = new PodcastTableRowFactory();
    //    $tableRow = $tableRowFactory->getTableRow(
    //        $xPath, $this->_tableName, $rssFeed
    //    );

    //    $this->_database->query(
    //        $tableRow->getUpdateSql("rss='" . $rssFeed . "'")
    //    );
    //}

    /**
     * Return if the RSS feed is in the database
     *
     * @param  string  RSS feed
     * @return boolean Is the RSS feed in the database
     */
    private function isRssFeedInDatabase($rssFeed)
    {
        $sql = "SELECT `id` FROM `podcasts` WHERE `rss`='$rssFeed'";
        return mysql_num_rows($this->_database->query($sql)) > 0;
    }
}
