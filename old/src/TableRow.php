<?php
class TableRow
{
    // Keep all data about the row in an array where the key is the column name 
    // and the value is the corresponding column data for the row. 
    private $_row = array();

    // Name of the table
    private $_tableName;

    /**
     * Construct the table row
     *
     * @param  string $tableName Name of the table
     */
    public function __construct($tableName)
    {
        $this->_tableName = $tableName;
    }

    /**
     * Add a column to the row
     *
     * @param       $column Name of the column
     * @param       $value  Column's value
     * @return void
     */
    public function add($column, $value)
    {
        $this->_row[$column] = $value;
    }

    /**
     * Get the value of a column.
     *
     * @param  $column Name of the column
     * @return         Value of the column, or null if the column doesn't exist
     */
    public function get($column)
    {
        if (isset($this->_row[$column]))
            return $this->_row[$column];
        else
            return null;
    }

    /**
     * Get the insert SQL
     *
     * @return string Insert SQL
     */
    public function getInsertSql()
    {
        // Prepare the columns
        $columns = "";
        foreach (array_keys($this->_row) as $key)
        {
            $columns .= mysql_real_escape_string($key) . ", ";
        }
        $columns = substr($columns, 0, -2);

        // Prepare the values that shall be inserted in the columns
        $values = "";
        foreach ($this->_row as $value)
        {
            $values .= "'" . mysql_real_escape_string($value) . "', ";
        }
        $values = substr($values, 0, -2);
        
        // Generate the SQL code
        return "INSERT INTO $this->_tableName ($columns) VALUES ($values)";
    }

    /**
     * Get the update SQL
     *
     * @return string Update SQL
     */
    public function getUpdateSql($where)
    {
        // Prepare the set statements
        $setStatements = "";
        foreach (array_keys($this->_row) as $key)
        {
            $setStatements .= $key . "='"
                . mysql_real_escape_string($this->_row[$key]) . "', ";
        }
        $setStatements = substr($setStatements, 0, -2);

        // Generate the SQL code
        return "UPDATE $this->_tableName SET $setStatements WHERE $where";
    }
}
