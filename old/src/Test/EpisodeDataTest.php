<?php
require_once(__DIR__ . '/../../lib/simpletest/autorun.php');
require_once(__DIR__ . '/../EpisodeData.php');

class EpisdeDataTest extends UnitTestCase
{
    function testGetMethods()
    {
        $xml = 
       "<?xml version='1.0'?>
        <item xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'
                xmlns:media='http://search.yahoo.com/mrss/'>
            <title>Section 9</title>
            <link>http://reddit.com/r/cyberpunk</link>
            <pubDate>Mon, 04 Jun 2012 10:59:02 -0400</pubDate>
            <enclosure url='http://saito.mp3' length='11738821'
                type='audio/mpeg' />
            <itunes:summary>summary</itunes:summary>
        </item>";

        // Prepare the EpisodeData
        $episodeData = $this->createEpisodeDataObject($xml);

        $this->assertEqual($episodeData->getNodeValue('title'), 'Section 9');
        $this->assertEqual($episodeData->getNodeValue('link'),
                           'http://reddit.com/r/cyberpunk');
        $this->assertEqual($episodeData->getNodeValue('itunes:summary'),
                           'summary');
        $this->assertEqual($episodeData->getMediaUrl(), 'http://saito.mp3');
        $this->assertEqual($episodeData->getMediaFileSize(), '11738821');
        $this->assertEqual($episodeData->getMediaType(), 'mp3');
        $this->assertEqual($episodeData->getPublishDate(), '2012-06-04 14:59:02');
    }

    function testGetPublishDate()
    {
        $this->publishDateTest(
            '2012-05-19T19:18:41-07:00',
            '2012-05-20 02:18:41',
            'pubDate'
        );
        $this->publishDateTest(
            'Sat, 17 Dec 2011 20:00:00 EST',
            '2011-12-18 01:00:00',
            'pubDate'
        );
        $this->publishDateTest(
            'Mon, 04 Jun 2012 10:59:02 -0400',
            '2012-06-04 14:59:02',
            'dc:date'
        );
        $this->publishDateTest(
            'Sun, 27 May 2012 17:00:08 +0000',
            '2012-05-27 17:00:08',
            'dc:date'
        );
        $this->publishDateTest(
            '',
            '',
            'doesnt exist'
        );
    }

    private function publishDateTest($publishDate, $correctValue, $publishTag)
    {
        $xml;
        switch ($publishTag) {
        case 'pubDate' :
            $xml = 
           "<?xml version='1.0'?>
            <item>
                <pubDate>" . $publishDate . "</pubDate>
            </item>";
            break;
        case 'dc:date' :
            $xml = 
           "<?xml version='1.0'?>
            <item xmlns:dc='http://purl.org/dc/elements/1.1/'>
                <dc:date>" . $publishDate . "</dc:date>
            </item>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <item>
            </item>";
            break;
        }

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getPublishDate(), $correctValue);
    }

    function testGetMediaUrl()
    {
        $this->mediaUrlTest(
            'http://link.mp3',
            'http://link.mp3',
            'enclosure'
        );
        $this->mediaUrlTest(
            'http://link.mp3',
            'http://link.mp3',
            'media:content'
        );
        $this->mediaUrlTest(
            '',
            '',
            'doesnt exist'
        );
    }

    private function mediaUrlTest($mediaUrl, $correctValue, $mediaTag)
    {
        $xml;
        switch ($mediaTag) {
        case 'enclosure' :
            $xml = 
           "<?xml version='1.0'?>
            <item>
                <enclosure url='" . $mediaUrl . "' />
            </item>";
            break;
        case 'media:content' :
            $xml = 
           "<?xml version='1.0'?>
            <item xmlns:media='http://search.yahoo.com/mrss/'>
                <media:content url='" . $mediaUrl . "' />
            </item>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <item>
            </item>";
            break;
        }

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getMediaUrl(), $correctValue);
    }

    function testGetMediaFileSize()
    {
        $this->mediaFileSizeTest('1000',   '1000',   'enclosure');
        $this->mediaFileSizeTest('999999', '999999', 'enclosure');
        $this->mediaFileSizeTest('1000',   '1000',   'media:content');
        $this->mediaFileSizeTest('999999', '999999', 'media:content');
        $this->mediaFileSizeTest('',       '',       'doesnt exist');
    }

    private function mediaFileSizeTest($fileSize, $correctValue, $mediaTag)
    {
        $xml;
        switch ($mediaTag) {
        case 'enclosure' :
            $xml = 
           "<?xml version='1.0'?>
            <item>
                <enclosure length='" . $fileSize . "' />
            </item>";
            break;
        case 'media:content' :
            $xml = 
           "<?xml version='1.0'?>
            <item xmlns:media='http://search.yahoo.com/mrss/'>
                <media:content fileSize='" . $fileSize . "' />
            </item>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <item>
            </item>";
            break;
        }

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getMediaFileSize(), $correctValue);
    }

    function testGetMediaType()
    {
        $this->mediaTypeTest('audio/ogg',   'ogg', 'enclosure');
        $this->mediaTypeTest('audio/mpeg',  'mp3', 'enclosure');
        $this->mediaTypeTest('audio/x-m4a', 'm4a', 'enclosure');
        $this->mediaTypeTest('audio/ogg',   'ogg', 'media:content');
        $this->mediaTypeTest('audio/mpeg',  'mp3', 'media:content');
        $this->mediaTypeTest('audio/x-m4a', 'm4a', 'media:content');
        $this->mediaTypeTest('',            '',    'doesnt exist');
    }

    private function mediaTypeTest($mediaType, $correctValue, $mediaTag)
    {
        $xml;
        switch ($mediaTag) {
        case 'enclosure' :
            $xml = 
           "<?xml version='1.0'?>
            <item>
                <enclosure type='" . $mediaType . "' />
            </item>";
            break;
        case 'media:content' :
            $xml = 
           "<?xml version='1.0'?>
            <item xmlns:media='http://search.yahoo.com/mrss/'>
                <media:content type='" . $mediaType . "' />
            </item>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <item>
            </item>";
            break;
        }

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getMediaType(), $correctValue);
    }

    function testGetDuration()
    {
        $this->durationTest('30',         '00:00:30');
        $this->durationTest('09:10',      '00:09:10');
        $this->durationTest('49:43',      '00:49:43');
        $this->durationTest('90:10',      '01:30:10');
        $this->durationTest('200:50',     '03:20:50');
        $this->durationTest('02:200:50',  '05:20:50');
        $this->durationTest('02:200:500', '05:28:20');
    }

    private function durationTest($duration, $correctDuration)
    {
        $xml = 
       "<?xml version='1.0'?>
        <item xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'>
            <itunes:duration>" . $duration . "</itunes:duration>
        </item>";

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getDuration(), $correctDuration);
    }

    function testExplicit()
    {
        $this->explicitTest('no',  'no');
        $this->explicitTest('yes', 'yes');
        $this->explicitTest('',    'no');
        $this->explicitTest('123', '123'); // For testing, to find out if there
                                           // are other tags than 'yes' and 'no'
    }

    private function explicitTest($explicit, $correctExplicit)
    {
        $xml = 
       "<?xml version='1.0'?>
        <item xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'>
            <itunes:explicit>" . $explicit. "</itunes:explicit>
        </item>";

        $episodeData = $this->createEpisodeDataObject($xml);
        $this->assertEqual($episodeData->getExplicit(), $correctExplicit);
    }

    private function createEpisodeDataObject($xml, $expression = '/item')
    {
        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xPath = new DOMXpath($doc);
        return new EpisodeData($xPath, $xPath->query($expression)->item(0));
    }
}
