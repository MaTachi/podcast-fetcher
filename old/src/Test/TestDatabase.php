<?php
require_once(__DIR__ . '/../Database.php');

class TestDatabase implements Database
{
    // Last SQL query
    private $_lastQuery;

    /**
     * Query the database with the given SQL code.
     *
     * @param  string  $sql SQL
     * @return              A MySQL resource
     */
    public function query($sql)
    {
        $this->_lastQuery = $sql;
    } 

    public function getLatestQuery()
    {
        return $this->_lastQuery;
    }
}
