<?php
require_once(__DIR__ . '/../../lib/simpletest/autorun.php');
require_once(__DIR__ . '/../PodcastData.php');

class PodcastDataTest extends UnitTestCase
{
    function testGetMethods()
    {
        $xml = 
       "<?xml version='1.0'?>
        <rss> 
            <channel>
                <node1>value</node1>
                <item>
                    <node2>value2</node2>
                </item>
                <node3>value3</node3>
                <node4>
                    <node5>123</node5>
                </node4>
            </channel>
        </rss>";

        $podcastData = $this->createPodcastDataObject($xml);

        $this->assertEqual($podcastData->getNodeValue('node1'), 'value');
        $this->assertEqual($podcastData->getNodeValue('node2'), '');
        $this->assertEqual($podcastData->getNodeValue('node3'), 'value3');
        $this->assertEqual($podcastData->getNodeValue('node4/node5'), '123');
    }

    function testGetLastUpdate()
    {
        $this->lastUpdateTest(
            '2012-05-19T19:18:41-07:00',
            '2012-05-20 02:18:41',
            'pubDate'
        );
        $this->lastUpdateTest(
            'Sat, 17 Dec 2011 20:00:00 EST',
            '2011-12-18 01:00:00',
            'pubDate'
        );
        $this->lastUpdateTest(
            'Mon, 04 Jun 2012 10:59:02 -0400',
            '2012-06-04 14:59:02',
            'lastBuildDate'
        );
        $this->lastUpdateTest(
            'Sun, 27 May 2012 17:00:08 +0000',
            '2012-05-27 17:00:08',
            'lastBuildDate'
        );
        $this->lastUpdateTest(
            '',
            '',
            'doesnt exist'
        );
    }

    private function lastUpdateTest($testValue, $correctValue, $tag)
    {
        $xml;
        switch ($tag) {
        case 'pubDate' :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
                <pubDate>" . $testValue . "</pubDate>
            </channel></rss>";
            break;
        case 'lastBuildDate' :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
                <lastBuildDate>" . $testValue . "</lastBuildDate>
            </channel></rss>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
            </channel></rss>";
            break;
        }

        $podcastData = $this->createPodcastDataObject($xml);
        $this->assertEqual($podcastData->getLastUpdated(), $correctValue);
    }

    function testGetImage()
    {
        $this->imageTest(
            'http://image.jpg',
            'http://image.jpg',
            'image/url'
        );
        $this->imageTest(
            'http://image.jpg',
            'http://image.jpg',
            'itunes:image'
        );
    }

    private function imageTest($testValue, $correctValue, $tag)
    {
        $xml;
        switch ($tag) {
        case 'image/url' :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
                <image>
                    <url>" . $testValue . "</url>
                </image>
            </channel></rss>";
            break;
        case 'itunes:image' :
            $xml = 
           "<?xml version='1.0'?>
            <rss xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'>
            <channel>
                <itunes:image href=\"" . $testValue . "\" />
            </channel></rss>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
            </channel></rss>";
            break;
        }

        $podcastData = $this->createPodcastDataObject($xml);
        $this->assertEqual($podcastData->getImage(), $correctValue);
    }

    function testGetLanguage()
    {
        $this->languageTest('sv',    'sv', 'language');
        $this->languageTest('sv-SE', 'sv', 'language');
        $this->languageTest('en-US', 'en', 'language');
        $this->languageTest('sv',    'sv', 'dc:language');
        $this->languageTest('sv-SE', 'sv', 'dc:language');
        $this->languageTest('en-US', 'en', 'dc:language');
        $this->languageTest('',      '',   'dc:language');
        $this->languageTest('',      '',   'doesnt exist');
    }

    private function languageTest($testValue, $correctValue, $tag)
    {
        $xml;
        switch ($tag) {
        case 'language' :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
                <language>" . $testValue . "</language>
            </channel></rss>";
            break;
        case 'dc:language' :
            $xml = 
           "<?xml version='1.0'?>
            <rss xmlns:dc='http://purl.org/dc/elements/1.1/'><channel>
                <dc:language>" . $testValue . "</dc:language>
            </channel></rss>";
            break;
        default :
            $xml = 
           "<?xml version='1.0'?>
            <rss><channel>
            </channel></rss>";
            break;
        }

        $podcastData = $this->createPodcastDataObject($xml);
        $this->assertEqual($podcastData->getLanguage(), $correctValue);
    }

    function testExplicit()
    {
        $this->explicitTest('no',  'no');
        $this->explicitTest('yes', 'yes');
        $this->explicitTest('',    'no');
        $this->explicitTest('123', '123'); // For testing, to find out if there
                                           // are other tags than 'yes' and 'no'
    }

    private function explicitTest($explicit, $correctExplicit)
    {
        $xml = 
       "<?xml version='1.0'?>
        <rss xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'><channel>
            <itunes:explicit>" . $explicit. "</itunes:explicit>
        </channel></rss>";

        $podcastData = $this->createPodcastDataObject($xml);
        $this->assertEqual($podcastData->getExplicit(), $correctExplicit);
    }

    private function createPodcastDataObject($xml)
    {
        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xPath = new DOMXpath($doc);
        return new PodcastData($xPath);
    }
}
