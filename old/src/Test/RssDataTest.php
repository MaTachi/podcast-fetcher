<?php
require_once(__DIR__ . '/../../lib/simpletest/autorun.php');
require_once(__DIR__ . '/../RssData.php');

class RssDataTest extends UnitTestCase
{
    function testGetMethods()
    {
        $xml = 
       "<?xml version='1.0'?>
        <rss xmlns:dc='http://purl.org/dc/elements/1.1/'>
            <node1>value</node1>
            <node2>another value</node2>
            <node3>
                <node4>third value</node4>
            </node3>
            <dc:date>date</dc:date>
        </rss>";

        // Prepare the XPath
        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xPath = new DOMXpath($doc);

        // Prepare the EpisodeData
        $rssData = new RssData($xPath, $xPath->document);

        // Single node value
        $this->assertEqual(
            $rssData->getNodeValue('rss/node1'),
            'value'
        );
        // Another single node
        $this->assertEqual(
            $rssData->getNodeValue('rss/node2'),
            'another value'
        );
        // A third value
        $this->assertEqual(
            $rssData->getNodeValue('rss/node3/node4'),
            'third value'
        );
        // Value of a node that doesn't exist
        $this->assertEqual(
            $rssData->getNodeValue('rss/node10'),
            ''
        );

        // Node with namespace
        $this->assertEqual(
            $rssData->getNodeValue('rss/dc:date'),
            'date'
        );

        // Multiple expression parameters, ordered by priority
        $this->assertEqual(
            $rssData->getNodeValue('rss/node10', 'rss/node2'),
            'another value'
        );

        // Get a node
        $this->assertEqual(
            $rssData->getNode('rss/node1')->nodeValue,
            'value'
        );
        // Get a node that doesn't exist
        $this->assertEqual(
            $rssData->getNode('rss/node10'),
            null
        );
    }
}
