<?php
require_once(__DIR__ . '/../../lib/simpletest/autorun.php');
require_once(__DIR__ . '/TestDatabase.php');
require_once(__DIR__ . '/../Podcasts.php');

class PodcastsTest extends UnitTestCase
{
    function testAddFromRssFeed()
    {
        $xml =
       "<?xml version='1.0' encoding='UTF-8'?>
        <rss
            xmlns:itunes='http://www.itunes.com/dtds/podcast-1.0.dtd'
            xmlns:media='http://search.yahoo.com/mrss/'
            xmlns:feedburner='http://rssnamespace.org/feedburner/ext/1.0'
            version='2.0'
        >
            <channel>
                <title>Slashat.se</title>
                <link>http://slashat.se</link>
                <description>Slashat.se är en svensk podcast...</description>
                <language>sv</language>
                <copyright>Jezper &amp; Tommie</copyright>
                <pubDate>Tue, 05 Jun 2012 21:25:55 +0200</pubDate>
                <itunes:author>Din machete i teknikdjungeln</itunes:author>
                <itunes:subtitle>Slashat.se är en svensk...</itunes:subtitle>
                <itunes:summary>Slashat.se är en...</itunes:summary>
                <itunes:image href='http://slashat.se/....jpg' />
                <itunes:explicit>no</itunes:explicit>
            </channel>
        </rss>";

        // Location of the rss file
        $rssFile = __DIR__ . '/' . 'rss.xml';

        // This is the correct SQL, i.e. what should be queried to the the
        // database from the XML file ($xml).
        $correctSql =
            "INSERT INTO podcasts (rss, title, link, description, language, "
          . "subtitle, summary, keywords, explicit, ownerName, ownerEmail, "
          . "image, author, lastUpdated, categories, added) VALUES ('"
          . $rssFile . "', 'Slashat.se', 'http://slashat.se', 'Slashat.se är "
          . "en svensk podcast...', 'sv', 'Slashat.se är en svensk...', "
          . "'Slashat.se är en...', '', 'no', 'Din machete i teknikdjungeln', "
          . "'', 'http://slashat.se/....jpg', 'Din machete i teknikdjungeln', "
          . "'2012-06-05 19:25:55', '', '2012-06-10 13:51:59')";

        // File handler
        $handle = fopen($rssFile, 'w');

        // Write the xml to the file
        fwrite($handle, $xml);

        // Create a mockup database
        $database = new TestDatabase();
        // Create a Podcasts object with the fake database
        $podcasts = new Podcasts($database);
        // Add data about the podcast from the RSS feed into the database
        $podcasts->addFromRssFeed($rssFile);

        // Finally check if the queried SQL is the same as the expected SQL
        //
        // substr is needed to remove the added date from both the query and 
        // the correct query, otherwise it would be hard to get a match.
        $this->assertEqual(
            substr($database->getLatestQuery(), 0, -21),
            substr($correctSql, 0, -21)
        );

        // Delete the rss file
        unlink($rssFile);
    }
}
