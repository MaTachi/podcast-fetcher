<?php
require_once(__DIR__ . '/../../lib/simpletest/autorun.php');
require_once(__DIR__ . '/../TableRow.php');

class TableRowTest extends UnitTestCase
{
    function testGetAddAndGet()
    {
        $tableRow = new TableRow('table');

        // Test a column that exist
        $tableRow->add('column', 'value');
        $this->assertEqual($tableRow->get('column'), 'value');

        // Test a column that doesn't exist
        $this->assertNull($tableRow->get('column2'));
    }

    function testGetInsertSql()
    {
        $tableRow = new TableRow('table');

        $insertSql = "INSERT INTO table () VALUES ()";
        $this->assertEqual($tableRow->getInsertSql(), $insertSql);

        $tableRow->add('motoko', 'kusanagi');
        $insertSql = "INSERT INTO table (motoko) VALUES ('kusanagi')";
        $this->assertEqual($tableRow->getInsertSql(), $insertSql);

        $tableRow->add('batou', 'tachikoma');
        $insertSql = "INSERT INTO table (motoko, batou) "
                   . "VALUES ('kusanagi', 'tachikoma')";
        $this->assertEqual($tableRow->getInsertSql(), $insertSql);
    }

    function testGetUpdateSql()
    {
        $tableRow = new TableRow('table');

        $updateSql = "UPDATE table SET  WHERE id=123";
        $this->assertEqual($tableRow->getUpdateSql('id=123'), $updateSql);

        $tableRow->add('motoko', 'kusanagi');
        $updateSql = "UPDATE table SET motoko='kusanagi' WHERE id=123";
        $this->assertEqual($tableRow->getUpdateSql('id=123'), $updateSql);

        $tableRow->add('batou', 'tachikoma');
        $updateSql = "UPDATE table SET motoko='kusanagi', batou='tachikoma' "
                   . "WHERE id=123";
        $this->assertEqual($tableRow->getUpdateSql('id=123'), $updateSql);
    }
}
