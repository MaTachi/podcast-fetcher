<?php
require_once('Database.php');
require_once('Table.php');
require_once('Podcasts.php');
require_once('EpisodeData.php');

class Episodes extends Table
{

    public function __construct(Database $database) {
        parent::__construct($database, 'episodes');
    }

    public function addFromRssFeed($rssFeed, $podcastId)
    {
        $doc = new DOMDocument();
        $doc->load($rssFeed);
        $xPath = new DOMXpath($doc);

        // Walk through the items in the RSS feed
        foreach ($xPath->query('/rss/channel/item') as $item)
        {
            // Episode Data
            $episodeData = new EpisodeData($xPath, $item);

            // Create a new table row
            $tableRow = new TableRow($this->_tableName);

            // Add data columns to the table row
            $tableRow->add('podcastId',
                           $podcastId);
            $tableRow->add('added',
                           gmdate('Y-m-d H:i:s'));
            $tableRow->add('title',
                           $episodeData->getNodeValue('title'));
            $tableRow->add('description',
                           $episodeData->getNodeValue('description'));
            $tableRow->add('link',
                           $episodeData->getNodeValue('link'));
            $tableRow->add('comments',
                           $episodeData->getNodeValue('comments'));
            $tableRow->add('pubDate',
                           $episodeData->getPublishDate());
            $tableRow->add('guid',
                           $episodeData->getNodeValue('guid'));
            $tableRow->add('mediaUrl',
                           $episodeData->getMediaUrl());
            $tableRow->add('mediaFileSize',
                           $episodeData->getMediaFileSize());
            $tableRow->add('mediaType',
                           $episodeData->getMediaType());
            $tableRow->add('itunesDuration',
                           $episodeData->getDuration());
            $tableRow->add('itunesSubtitle',
                           $episodeData->getNodeValue('itunes:subtitle'));
            $tableRow->add('itunesSummary',
                           $episodeData->getNodeValue('itunes:summary'));
            $tableRow->add('itunesKeywords',
                           $episodeData->getNodeValue('itunes:keywords'));
            $tableRow->add('itunesExplicit',
                           $episodeData->getExplicit());
            $tableRow->add('feedburnerOrigEnclosureLink',
                           $episodeData
                               ->getNodeValue('feedburner:origEnclosureLink'));

            if ($this->isEpisodeInDatabase($tableRow))
            {
                break;
            }

            // Run the mysql query
            $this->_database->query($tableRow->getInsertSql());
        }
    }
    
    public function isEpisodeInDatabase($tableRow)
    {
        $sql = "SELECT * FROM episodes WHERE guid='"
             . mysql_real_escape_string($tableRow->get('guid')) . "' AND "
             . "podcastId='"
             . mysql_real_escape_string($tableRow->get('podcastId')) . "'";
        return mysql_num_rows($this->_database->query($sql)) == 1;
        //$this->_database->query(
        //    $tableRow->getUpdateSql(
        //        "guid='"
        //        . mysql_real_escape_string($tableRow->get('guid'))
        //        . "'"
        //    )
        //);
    }
}
