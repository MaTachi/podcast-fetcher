<?php

class PodcastsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $podcastTable = new Application_Model_DbTable_Podcast();
        $podcasts = $podcastTable->find(array(1, 2));
        foreach ($podcasts as $podcast) {
            echo "{$podcast->title} : {$podcast->rss} </br>";
        }
    }

}

