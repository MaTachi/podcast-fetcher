# README

## Description

This project lets you fetch data about podcasts and all their  episodes from RSS
feeds. It then stores this in a database and provides an API for getting the
data.

## Installation

### Install Apache, MySQL and PHP

If you want to run the application locally, check out
[this site](https://help.ubuntu.com/community/ApacheMySQLPHP) which contains
instructions on how to install Apache, MySQL and PHP on Ubuntu.

### Install Zend Framework

1. Download `Zend Framework 1.11 full (recommended)` from
   [Zend.com](http://www.zend.com/community/downloads).

2. Unpack the folder somewhere on your computer.

3. Create a symbolic link in the project's library folder to the unpacked Zend
   folder with this terminal command:

       `ln -s /home/user/libraries/ZendFramework/library/Zend /home/user/projects/podcast-site/library/Zend`

### Install the "zf" script from Zend Framework

    sudo apt-get install zend-framework-bin

### Setting Up Your VHOST

The following is a sample VHOST you might want to consider for your project.

    <VirtualHost *:80>
        DocumentRoot "/home/user/projects/podcast-site/public"
        ServerName podcast-site.local

        # This should be omitted in the production environment
        SetEnv APPLICATION_ENV development

        <Directory "/home/user/projects/podcast-site/public">
             Options Indexes MultiViews FollowSymLinks
             AllowOverride All
             Order allow,deny
             Allow from all
        </Directory>
    </VirtualHost>

[Here](https://www.digitalocean.com/community/articles/how-to-set-up-apache-virtual-hosts-on-ubuntu-12-04-lts)
is a neat site with instructions.

## Tests

### Requirements to run the tests

Use one of the following methods to install PHPUnit:

* Install PHPUnit through PEAR
  [with these instructions](http://www.phpunit.de/manual/3.6/en/installation.html).

* Install PHPUnit through the package manager `sudo apt-get install phpunit`.

### How to run the tests

Run the following line in the terminal:

    /opt/lampp/bin/phpunit -c /path/to/project/folder/phpunit.xml.dist

## API

...
